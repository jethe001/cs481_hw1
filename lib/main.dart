import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hello World App by Vinayak',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hello World App by Vinayak'),
        ),
        body: Center(
          child: Text("Hello World!"
          "\nName: Vinayak Jayajee Jethe"
          "\nExpected Graduation: Fall 2021"
          "\nFavorite Quote: Don't just exist, live."),
        ),
      ),
    );
  }
}
